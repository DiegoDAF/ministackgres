# miniStackGres

running StackGres on minikube!

```
   _____ _             _     _____                   
  / ____| |           | |   / ____|                  
 | (___ | |_ __ _  ___| | _| |  __ _ __ ___  ___     
  \___ \| __/ _` |/ __| |/ / | |_ | '__/ _ \/ __|    
  ____) | || (_| | (__|   <| |__| | | |  __/\__ \    
 |_____/ \__\__,_|\___|_|\_\\_____|_|  \___||___/    
                                  by OnGres, Inc. 
```


## Getting started

Install minikube
Install kubernetes
Install Helm
Install Helmfile

## Installing!

```
minikube delete 
minikube start
```

```
helmfile -f 01-operators/helmfile.yaml apply
```

## Install messages:

```
Release "prometheus-operator" does not exist. Installing it now.  
NAME: prometheus-operator      
LAST DEPLOYED: Mon Sep 19 16:23:45 2022              
NAMESPACE: monitoring          
STATUS: deployed               
REVISION: 1                    
NOTES:   

kube-prometheus-stack has been installed. Check its status by running:                  
  kubectl --namespace monitoring get pods -l "release=prometheus-operator"              
         
Visit https://github.com/prometheus-operator/kube-prometheus for instructions on how to create & configure Alertmanager and Prometheus instances using the Operator.   
         
Listing releases matching ^prometheus-operator$      
prometheus-operator     monitoring      1               2022-09-19 16:23:45.189759685 -0300 -03 deployed        kube-prometheus-stack-40.1.0    0.59.1                 
Release "stackgres-operator" does not exist. Installing it now.   
NAME: stackgres-operator       
LAST DEPLOYED: Mon Sep 19 16:23:41 2022              
NAMESPACE: stackgres           
STATUS: deployed               
REVISION: 1                    
TEST SUITE: None               
NOTES:   
Release Name: stackgres-operator                     
StackGres Version: 1.3.1       
         
   _____ _             _     _____                   
  / ____| |           | |   / ____|                  
 | (___ | |_ __ _  ___| | _| |  __ _ __ ___  ___     
  \___ \| __/ _` |/ __| |/ / | |_ | '__/ _ \/ __|    
  ____) | || (_| | (__|   <| |__| | | |  __/\__ \    
 |_____/ \__\__,_|\___|_|\_\\_____|_|  \___||___/    
                                  by OnGres, Inc.   

Check if the operator was successfully deployed and is available:

    kubectl describe deployment -n stackgres stackgres-operator

    kubectl wait -n stackgres deployment/stackgres-operator --for condition=Available

Check if the restapi was successfully deployed and is available:

    kubectl describe deployment -n stackgres stackgres-restapi

    kubectl wait -n stackgres deployment/stackgres-restapi --for condition=Available

To access StackGres Operator UI from localhost, run the below commands:

    POD_NAME=$(kubectl get pods --namespace stackgres -l "app=stackgres-restapi" -o jsonpath="{.items[0].metadata.name}")

    kubectl port-forward "$POD_NAME" 8443:9443 --namespace stackgres

Read more about port forwarding here: http://kubernetes.io/docs/user-guide/kubectl/kubectl_port-forward/

Now you can access the StackGres Operator UI on:

    https://localhost:8443

To get the username, run the command:

    kubectl get secret -n stackgres stackgres-restapi --template '{{ printf "username = %s\n" (.data.k8sUsername | base64decode) }}'

To get the generated password, run the command:

    kubectl get secret -n stackgres stackgres-restapi --template '{{ printf "password = %s\n" (.data.clearPassword | base64decode) }}'

Remember to remove the generated password hint from the secret to avoid security flaws:

    kubectl patch secrets --namespace stackgres stackgres-restapi --type json -p '[{"op":"remove","path":"/data/clearPassword"}]'

```

## Just set up a password that I know.

```
    kubectl apply -n stackgres -f password.patch

    kubectl delete pod -n stackgres $POD_NAME
    POD_NAME=$(kubectl get pods --namespace stackgres -l "app=stackgres-restapi" -o jsonpath="{.items[0].metadata.name}")
    kubectl port-forward "$POD_NAME" 8443:9443 --namespace stackgres &
```

## Installing and setuping StackGres
```
kubectl apply -f ./02-configs/01-CreateNameSpaces.yaml
kubectl apply -f ./02-configs/02-StorageClass.yaml      
kubectl apply -f ./02-configs/03-SGInstanceProfile.yaml 
kubectl apply -f ./02-configs/04-SGPostgresConfig-14.yaml
kubectl apply -f ./02-configs/05-SGPoolingConfig.yaml
```
Number 06 is the backup... WIP.
```
kubectl apply -f ./02-configs/07-SGDistributedLogs.yaml  
```

## Creating a PostgreSQL Cluster called demo-db
```
kubectl apply -f ./02-configs/08-sg-cdb.yaml  
```

## Creating a PostgreSQL Cluster called demo-db
```
kubectl apply -f ./03-cluster/01-SGCluster-daf.yaml
```

```
kubectl get pods -n demo-db  -o wide --watch
kubectl get events -n demo-db -w | grep demo

wait

wait

wait
```

## Storage are in delete... 
```
kubectl get pvc -n demo-db
kubectl get pv -n demo-db #check if retain policy is reatin... delete is bad.
```

## change this
```
kubectl patch pv pvc-c549fc71-df44-47e2-9851-c9a9aa4f6d47 -p '{"spec":{"persistentVolumeReclaimPolicy":"Retain"}}'
```

## Access to the DB via psql
```
kubectl exec -ti -n demo-db demo-db-0 -c postgres-util -- psql
```



